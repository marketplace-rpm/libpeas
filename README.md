# Information / Информация

SPEC-файл для создания RPM-пакета **libpeas**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/libs`.
2. Установить пакет: `dnf install libpeas`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)